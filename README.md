# langtrans-app

## Project setup
```
git clone https://gitlab.com/mesayusriana12/langtrans-app.git

cd langtrans-app

npm install
```

### Run local server
```
npm run serve
```
### Credits

* [Translate API](https://github.com/azharimm/api-translate)